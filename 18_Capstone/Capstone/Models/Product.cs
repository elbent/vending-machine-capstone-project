﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Capstone.Models
{
    public class Product
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Product"/> class.
        /// </summary>
        /// <param name="name">The name of the product.</param>
        /// <param name="price">The price in dollars of the product.</param>
        /// <param name="category">The category of the product.</param>
        /// <param name="slotNumber">The machine slot number the product will go in.</param>
        public Product(string name, decimal price, string category, string slotNumber)
        {
            this.Name = name;
            this.Price = price;
            this.Category = category;
            this.SlotNumber = slotNumber;
        }

        /// <summary>
        /// Gets the name of the product.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets the price in dollars of the product.
        /// </summary>
        public decimal Price { get; }

        /// <summary>
        /// Gets the category of the product.
        /// </summary>
        public string Category { get; }

        /// <summary>
        /// Gets the machine slot number the product will go in.
        /// </summary>
        public string SlotNumber { get; }

        /// <summary>
        /// Gets a reaction phrase based on what category the product is.
        /// </summary>
        /// <returns>The phrase to be printed for this product.</returns>
        public override string ToString()
        {
            switch (this.Category)
            {
                case "Chip":
                    return "Crunch Crunch, Yum!";
                case "Candy":
                    return "Munch Munch, Yum!";
                case "Drink":
                    return "Glug Glug, Yum!";
                case "Gum":
                    return "Chew Chew, Yum!";
                default:
                    return "Yum!";
            }
        }
    }
}
