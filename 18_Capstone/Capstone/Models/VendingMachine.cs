﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Capstone.Models
{
    public class VendingMachine
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VendingMachine"/> class.
        /// </summary>
        /// <param name="filePath">Takes in a string where the inventory file is located</param>
        public VendingMachine(string filePath)
        {
            this.Products = new Dictionary<string, Product>();
            this.Quantities = new Dictionary<string, int>();
            this.ProductsBought = new List<Product>();
            this.InventoryFilePath = filePath;
            this.MoneyFed = 0;
            this.Load();
        }

        /// <summary>
        /// Gets array of procducts this user has bought.
        /// </summary>
        public Product[] ProductsBoughtList
        {
            get
            {
                return this.ProductsBought.ToArray();
            }
        }

        /// <summary>
        /// Gets the amount of money the current user has fed into the machine.
        /// </summary>
        public decimal MoneyFed { get; private set; }

        /// <summary>
        /// Gets the dictionary that maps the product to the amount that we have in stock.
        /// </summary>
        public Dictionary<string, int> Quantities { get; private set; }

        /// <summary>
        /// Gets the dictionary that maps the slot location to the products that are there.
        /// </summary>
        public Dictionary<string, Product> Products { get; }

        /// <summary>
        /// Gets amount of products user has bought this session in a private list.
        /// </summary>
        private List<Product> ProductsBought { get; }

        /// <summary>
        /// Gets the file path to the inventory file.
        /// </summary>
        private string InventoryFilePath { get; }

        /// <summary>
        /// Loads machine inventory info from file at filePath.
        /// </summary>
        public void Load()
        {
            using (StreamReader sr = new StreamReader(this.InventoryFilePath))
            {
                while (!sr.EndOfStream)
                {
                    string[] productInfo = sr.ReadLine().Split("|");
                    string productSlot = productInfo[0];
                    string productName = productInfo[1];
                    decimal productPrice = decimal.Parse(productInfo[2]);
                    string productCategory = productInfo[3];
                    Product productToAdd = new Product(productName, productPrice, productCategory, productSlot);

                    if (!this.Quantities.ContainsKey(productName))
                    {
                        // If we haven't seen this product before, add it to products AND make new entry in quantities.
                        this.Products.Add(productSlot, productToAdd);
                        this.Quantities.Add(productName, 1);
                    }
                    else
                    {
                        // If we have seen this product before, update quantity.
                        this.Quantities[productName]++;
                    }
                }
            }
        }

        /// <summary>
        /// Updates the quantity of Product, current user's money total, and adds to ProductsBought.
        /// </summary>
        /// <param name="product">The product to be purchased.</param>
        public void MakePurchase(Product product)
        {
            this.Quantities[product.Name]--;
            this.MoneyFed -= product.Price;
            this.ProductsBought.Add(product);
        }

        /// <summary>
        /// Adds amount to money user already has.
        /// </summary>
        /// <param name="amount">The amount of money to add.</param>
        /// <returns>Returns the user's money after more has been added.</returns>
        public decimal FeedMoney(decimal amount)
        {
            this.MoneyFed += amount;
            return this.MoneyFed;
        }

        /// <summary>
        /// Takes in an amount of money and outputs the number of quarters, dimes, and nickels
        /// needed to make change.
        /// </summary>
        /// <param name="amount">The amount of money to make change for.</param>
        /// <returns>An array with number of quarters at position 0, number of dimes at position 1,
        /// and number of nickels at position 2.</returns>
        public int[] GetChange(decimal amount)
        {
            int quarters = 0;
            int dimes = 0;
            int nickels = 0;
            amount *= 100;

            while (amount > 0)
            {
                if (amount - 25 >= 0)
                {
                    quarters++;
                    amount -= 25;
                    continue;
                }
                else if (amount - 10 >= 0)
                {
                    dimes++;
                    amount -= 10;
                    continue;
                }
                else if (amount - 5 >= 0)
                {
                    nickels++;
                    amount -= 5;
                    continue;
                }
            }

            this.MoneyFed = 0;
            int[] coins = new int[3] { quarters, dimes, nickels };
            return coins;
        }

        /// <summary>
        /// Writes timestamp, transaction type, balance before the transaction, and balance after
        /// transaction to a log file called Log.txt.
        /// </summary>
        /// <param name="message">The message to print in the log file.</param>
        /// <param name="startingBalance">The current user's money before the transaction.</param>
        /// <param name="endingBalance">The current user's money after the transaction.</param>
        public void WriteToAuditLog(string message, decimal startingBalance, decimal endingBalance)
        {
            using (StreamWriter sw = new StreamWriter("./Log.txt", true))
            {
                string lineToWrite = string.Format(
                    "{0,-15}\t{1,-15}\t{2,-5 :C}\t{3,-5 :C}", DateTime.Now.ToString(), message, startingBalance, endingBalance);
                sw.WriteLine(lineToWrite);
            }
        }
    }
}
