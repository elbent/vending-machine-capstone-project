﻿using System;
using System.Collections.Generic;
using System.Text;
using Capstone.Models;

namespace Capstone.Views
{
    public abstract class CLIMenu
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CLIMenu"/> class.
        /// <param name="machine">Takes in a string where the inventory file is located.</param>        /// </summary>
        public CLIMenu(VendingMachine machine)
        {
            this.Machine = machine;
            this.MenuOptions = new Dictionary<string, string>();
        }

        /// <summary>
        /// Gets or sets the title of this menu.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets the vending machine that this menu is operating on.
        /// </summary>
        protected VendingMachine Machine { get; }

        /// <summary>
        /// Gets menu options display to the user.
        /// </summary>
        protected Dictionary<string, string> MenuOptions { get; }

        /// <summary>
        /// Run starts the menu loop.
        /// </summary>
        public void Run()
        {
            Console.Clear();
            while (true)
            {
                Console.WriteLine(this.Title);
                Console.WriteLine(new string('=', this.Title.Length));
                Console.WriteLine("\r\nPlease make a selection:");
                foreach (KeyValuePair<string, string> menuItem in this.MenuOptions)
                {
                    Console.WriteLine($"{menuItem.Key} - {menuItem.Value}");
                }

                Console.WriteLine($"Current Money Provided: {this.Machine.MoneyFed :C}");
                string choice = this.GetString("Selection:").ToUpper();

                if (this.MenuOptions.ContainsKey(choice))
                {
                    if (choice == "Q")
                    {
                        break;
                    }

                    if (!this.ExecuteSelection(choice))
                    {
                        break;
                    }
                } else
                {
                    Console.Clear();
                }
            }
        }

        /// <summary>
        /// Given a valid menu selection, runs the approriate code to do what the user is asking for.
        /// </summary>
        /// <param name="choice">The menu option (key) selected by the user</param>
        /// <returns>True to keep executing the menu (loop), False to exit this menu (break)</returns>
        protected abstract bool ExecuteSelection(string choice);

        /// <summary>
        /// Displays the stocked items to the user.
        /// </summary>
        /// <returns>Returns true to keep menu loop going.</returns>
        protected bool DisplayItems()
        {
            Console.Clear();
            Console.WriteLine("{0,-7}{1,-20}{2,-15}{3,-15}", "Slot", "Name", "Price", "Quantity");
            string quantity = string.Empty;
            foreach (KeyValuePair<string, Product> entry in this.Machine.Products)
            {
                // If the quantity of this item is 0, print SOLD OUT
                if (this.Machine.Quantities[entry.Value.Name] == 0)
                {
                    quantity = "SOLD OUT";
                }
                else
                {
                    quantity = this.Machine.Quantities[entry.Value.Name].ToString();
                }

                Console.WriteLine("{0,-7}{1,-20}{2,-15}{3,-15}", entry.Key, entry.Value.Name, entry.Value.Price, quantity);
            }

            return true;
        }

        /// <summary>
        /// This continually prompts the user until they enter a valid integer.
        /// </summary>
        /// <param name="message">Message to print when prompting for input.</param>
        /// <returns>Returns int that user entered.</returns>
        protected int GetInteger(string message)
        {
            int resultValue = 0;
            while (true)
            {
                Console.Write(message + " ");
                string userInput = Console.ReadLine().Trim();
                if (int.TryParse(userInput, out resultValue))
                {
                    break;
                }
                else
                {
                    Console.WriteLine("!!! Invalid input. Please enter a valid whole number.");
                }
            }

            return resultValue;
        }

        /// <summary>
        /// This continually prompts the user until they enter a valid double.
        /// </summary>
        /// <param name="message">Message to print when prompting for input.</param>
        /// <returns>Returns double that user entered.</returns>
        protected double GetDouble(string message)
        {
            double resultValue = 0;
            while (true)
            {
                Console.Write(message + " ");
                string userInput = Console.ReadLine().Trim();
                if (double.TryParse(userInput, out resultValue))
                {
                    break;
                }
                else
                {
                    Console.WriteLine("!!! Invalid input. Please enter a valid decimal number.");
                }
            }

            return resultValue;
        }

        /// <summary>
        /// This continually prompts the user until they enter a valid bool.
        /// </summary>
        /// <param name="message">Message to print when prompting for input.</param>
        /// <returns>Returns boolean that user entered.</returns>
        protected bool GetBool(string message)
        {
            bool resultValue = false;
            while (true)
            {
                Console.Write(message + " ");
                string userInput = Console.ReadLine().Trim();
                if (userInput.ToUpper() == "Y")
                {
                    resultValue = true;
                    break;
                }
                else if (userInput == "N")
                {
                    resultValue = false;
                    break;
                }
                else if (bool.TryParse(userInput, out resultValue))
                {
                    break;
                }
                else
                {
                    Console.WriteLine("!!! Invalid input. Please enter [True, False, Y or N].");
                }
            }
            return resultValue;
        }

        /// <summary>
        /// This continually prompts the user until they enter a valid string (1 or more characters).
        /// </summary>
        /// <param name="message">Message to print when prompting for input.</param>
        /// <returns>Returns string that user entered</returns>
        protected string GetString(string message)
        {
            while (true)
            {
                Console.Write(message + " ");
                string userInput = Console.ReadLine().Trim();
                if (!string.IsNullOrEmpty(userInput))
                {
                    return userInput;
                }
                else
                {
                    Console.WriteLine("!!! Invalid input. Please enter a valid decimal number.");
                }
            }
        }

        /// <summary>
        /// Shows a message to the user and waits for the user to hit return.
        /// </summary>
        /// <param name="message">Message to print when prompting for input.</param>
        protected void Pause(string message)
        {
            Console.Write(message + " Press Enter to continue.");
            Console.ReadLine();
        }
    }
}
