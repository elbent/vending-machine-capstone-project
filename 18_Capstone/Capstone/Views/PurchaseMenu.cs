﻿using System;
using System.Collections.Generic;
using System.Text;
using Capstone.Models;

namespace Capstone.Views
{
    public class PurchaseMenu : CLIMenu
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PurchaseMenu"/> class.
        /// </summary>
        /// <param name="vendingMachineReference">Pointer to the instance of vending machine this menu uses</param>
        public PurchaseMenu(VendingMachine vendingMachineReference)
            : base(vendingMachineReference)
        {
            this.Title = "Purchase Menu";
            this.MenuOptions.Add("1", "Feed Money");
            this.MenuOptions.Add("2", "Select Product");
            this.MenuOptions.Add("3", "Finish Transaction");
        }

        /// <summary>
        /// Takes in a user input and executes proper menu selection.
        /// </summary>
        /// <param name="choice">The key entered by the user to execute.</param>
        /// <returns>Returns true to continue the menu loop.</returns>
        protected override bool ExecuteSelection(string choice)
        {
            Console.Clear();
            switch (choice)
            {
                case "1":
                    decimal amount = Convert.ToDecimal(this.GetDouble("Please enter the whole dollar amount of money to add: "));
                    if (amount < 0)
                    {
                        Console.WriteLine("Entered invalid amount of money.");
                    }
                    else
                    {
                        decimal balance = this.Machine.MoneyFed;
                        this.Machine.FeedMoney(amount);
                        this.Machine.WriteToAuditLog("FEED MONEY:", balance, this.Machine.MoneyFed);
                    }

                    return true;
                case "2":
                    this.DisplayItems();
                    Console.WriteLine("Please enter the product slot of the product you wish to purchase");
                    string input = Console.ReadLine().ToUpper();

                    // If the quantity of the item is 0, or the Item they Input does not exist, return them to purchase menu.
                    Console.Clear();
                    if (!this.Machine.Products.ContainsKey(input))
                    {
                        Console.WriteLine("Entered invalid input, returning to purchase menu");
                        return true;
                    }

                    Product currentProduct = this.Machine.Products[input];
                    if (this.Machine.Quantities[currentProduct.Name] == 0)
                    {
                        Console.WriteLine("This product has sold out; please select a different item.");
                    }
                    else if (this.Machine.MoneyFed < currentProduct.Price)
                    {
                        Console.WriteLine("Insufficiant amount of money; please add more or select different item.");
                        return true;
                    }
                    else
                    {
                        decimal oldBalance = this.Machine.MoneyFed;
                        this.Machine.MakePurchase(currentProduct);
                        this.Machine.WriteToAuditLog(
                            currentProduct.Name + " " + currentProduct.SlotNumber,
                            oldBalance,
                            this.Machine.MoneyFed);
                        Console.WriteLine($"You purchased a {currentProduct.Name} for {currentProduct.Price:C}");
                    }

                    return true;
                case "3":
                    Console.Clear();
                    int[] coins = this.Machine.GetChange(this.Machine.MoneyFed);
                    decimal startingBalance = this.Machine.MoneyFed;
                    Console.WriteLine("Getting change.....");
                    Console.WriteLine($"You received {this.Machine.MoneyFed:C} in {coins[0]} quarters, " +
                        $"{coins[1]} dimes, and {coins[2]} nickels");
                    this.Machine.WriteToAuditLog("GIVE CHANGE: ", startingBalance, this.Machine.MoneyFed);

                    foreach (Product product in this.Machine.ProductsBoughtList)
                    {
                        Console.WriteLine(product.ToString());
                    }

                    this.Pause("Transaction ended.");
                    Console.Clear();
                    return false;
            }

            Console.Clear();
            return true;
        }
    }
}
