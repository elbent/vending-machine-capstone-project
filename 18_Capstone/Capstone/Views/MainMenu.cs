﻿using Capstone.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Capstone.Views
{
    /// <summary>
    /// The top-level menu in our Market Application.
    /// </summary>
    public class MainMenu : CLIMenu
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainMenu"/> class.
        /// Constructor adds items to the top-level menu.
        /// <param name="vendingMachineReference">Reference to vending machine this menu is running on.</param>
        /// </summary>
        public MainMenu(VendingMachine vendingMachineReference) : base(vendingMachineReference)
        {
            this.Title = "*** Main Menu ***";
            this.MenuOptions.Add("1", "Display Vending Machine Items");
            this.MenuOptions.Add("2", "Purchase");
            this.MenuOptions.Add("Q", "Quit");
        }

        /// <summary>
        /// The override of ExecuteSelection handles whatever selection was made by the user.
        /// This is where any business logic is executed.
        /// </summary>
        /// <param name="choice">"Key" of the user's menu selection</param>
        /// <returns>Returns true to continue the menu loop.</returns>
        protected override bool ExecuteSelection(string choice)
        {
            switch (choice)
            {
                case "1":
                    this.DisplayItems();
                    return true;
                case "2":
                    // open the purchase menu
                    PurchaseMenu menu = new PurchaseMenu(this.Machine);
                    menu.Run();
                    return true;
            }

            return true;
        }
    }
}
