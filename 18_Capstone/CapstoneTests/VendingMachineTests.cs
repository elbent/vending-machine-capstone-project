using Capstone.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CapstoneTests
{
    [TestClass]
    public class VendingMachineTests
    {
        [DataTestMethod]
        [DataRow(3.00)]
        [DataRow(1.50)]
        public void TestFeedMoney(double amount)
        {
            VendingMachine machine = new VendingMachine(@"C:..\..\..\..\etc\vendingmachine.csv");

            Assert.AreEqual(Convert.ToDecimal(amount), machine.FeedMoney(Convert.ToDecimal(amount)));
        }

        [DataTestMethod]
        [DataRow(1.50, new int[3] { 6, 0, 0 })]
        [DataRow(2.15,new int[3] { 8, 1, 1 })]
        [DataRow(.40, new int[3] { 1, 1, 1 })]
        [DataRow(.05, new int[3] { 0, 0, 1 })]
        [DataRow(.10, new int[3] { 0, 1, 0 })]
        [DataRow(.25, new int[3] { 1, 0, 0 })]
        [DataRow(.15, new int[3] { 0, 1, 1 })]
        public void TestChange(double amount, int[] coins)
        {
            VendingMachine machine = new VendingMachine(@"C:..\..\..\..\etc\vendingmachine.csv");

            int[] result = machine.GetChange(Convert.ToDecimal(amount));
            CollectionAssert.AreEqual(coins, result, $"Your change is {result[0]}, {result[1]}, {result[2]}");
        }

        [TestMethod]
        public void TestMakePurchase()
        {
            VendingMachine machine = new VendingMachine(@"C:..\..\..\..\etc\vendingmachine.csv");
            machine.FeedMoney(5.00M);
            Product product = new Product("Potato Crisps", 3.05M, "Chip", "A1");
            machine.MakePurchase(product);

            Assert.AreEqual(4, machine.Quantities[product.Name], "Product quantity should be reduced by 1");
            Assert.AreEqual(1.95M, machine.MoneyFed, "Money fed should be reduced by product price.");
            Assert.AreEqual(product, machine.ProductsBoughtList[0], "Product should have been added to products bought");
        }
    }
}
